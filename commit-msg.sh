#!/bin/bash

#place this file in commit file inside .git/hooks/

c_msg=$1
msg=$(cat "$c_msg")

echo "$msg"
req_words=("commit-summary" "commit-details")

for word in "${req_words[@]}"; do
        
        if ! echo "$msg" | grep -q "$word"; then
                echo "error while commit ... :("
                echo "$word is required in your commit message ..!"
                exit 1
        fi
done
